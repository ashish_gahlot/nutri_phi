package com.example.NutriPhi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ContactUs extends AppCompatActivity {
    TextView phone_one;
    TextView phone_two;
    TextView email;
    TextView map;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        phone_one = findViewById(R.id.txt_3);
        phone_two = findViewById(R.id.txt_4);
        email = findViewById(R.id.txt_5);
        map = findViewById(R.id.txt_2);

        final String num_one = phone_one.getText().toString();
        final String num_two = phone_two.getText().toString();
        final String map_info = map.getText().toString();


        phone_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_DIAL);
                i.setData(Uri.parse("tel:"+num_one));
                startActivity(i);
            }
        });

        phone_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_DIAL);
                i.setData(Uri.parse("tel:"+num_two));
                startActivity(i);
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String mail = email.getText().toString();
                String[] rec = mail.split(",");
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL, rec);
                intent.setType("message/rfc822");
                startActivity(intent.createChooser(intent, "Choose an email client"));
            }
        });

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("geo:28.449133,76.999086"));
                startActivity(intent.createChooser(intent, "Launch Maps"));
            }
        });

    }
}
