package com.example.NutriPhi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Cattle_Menu extends AppCompatActivity {
    CardView cardView_1, cardView_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cattle__menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cardView_1 = findViewById(R.id.cattle_info);
        cardView_2 = findViewById(R.id.feeding_cattle);

        cardView_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_1 = new Intent(Cattle_Menu.this, Cattle_info.class);
                startActivity(intent_1);
            }
        });

        cardView_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_2 = new Intent(Cattle_Menu.this, Feeding_Cattle.class);
                startActivity(intent_2);
            }
        });
    }
}
