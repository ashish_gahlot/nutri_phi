package com.example.NutriPhi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Cow extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final RadioGroup radioGroup;
        final TextView textView_1, textView_2, textView_3;
        final RadioButton btn_1, btn_2, btn_3;
        Spinner milk_yield;
        milk_yield = findViewById(R.id.spinner);
        btn_1 = findViewById(R.id.high);
        btn_2 = findViewById(R.id.moderate);
        btn_3 = findViewById(R.id.low);

        textView_1 = findViewById(R.id.concentrate);
        textView_2 = findViewById(R.id.green_fodder);
        textView_3 = findViewById(R.id.dry_roughage);

        radioGroup = findViewById(R.id.radio_group);

        milk_yield.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getSelectedItem().toString().trim();
                switch (selected)
                {
                    case "5":
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if(checkedId == btn_1.getId())
                                {
                                        textView_1.setText("0 Kg");
                                        textView_2.setText("35 Kg");
                                        textView_3.setText("3.5 Kg");
                                }
                                if(checkedId == btn_2.getId())
                                {
                                    textView_1.setText("1.8 Kg");
                                    textView_2.setText("20 Kg");
                                    textView_3.setText("3 Kg");
                                }
                                if(checkedId == btn_3.getId())
                                {
                                    textView_1.setText("5.3 Kg");
                                    textView_2.setText("2 Kg");
                                    textView_3.setText("4 Kg");
                                }
                            }
                        });
                        break;
                    case "7.5":
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if(checkedId == btn_1.getId())
                                {
                                    textView_1.setText("1.2 Kg");
                                    textView_2.setText("35 Kg");
                                    textView_3.setText("3.5 Kg");
                                }
                                if(checkedId == btn_2.getId())
                                {
                                    textView_1.setText("3 Kg");
                                    textView_2.setText("20 Kg");
                                    textView_3.setText("3 Kg");
                                }
                                if(checkedId == btn_3.getId())
                                {
                                    textView_1.setText("6.5 Kg");
                                    textView_2.setText("2 Kg");
                                    textView_3.setText("4 Kg");
                                }
                            }
                        });
                        break;
                    case "10":
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if(checkedId == btn_1.getId())
                                {
                                    textView_1.setText("2.4 Kg");
                                    textView_2.setText("35 Kg");
                                    textView_3.setText("3.5 Kg");
                                }
                                if(checkedId == btn_2.getId())
                                {
                                    textView_1.setText("4.2 Kg");
                                    textView_2.setText("20 Kg");
                                    textView_3.setText("3 Kg");
                                }
                                if(checkedId == btn_3.getId())
                                {
                                    textView_1.setText("8 Kg");
                                    textView_2.setText("2 Kg");
                                    textView_3.setText("4.5 Kg");
                                }
                            }
                        });
                        break;
                    case "12.5":
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if(checkedId == btn_1.getId())
                                {
                                    textView_1.setText("3.6 Kg");
                                    textView_2.setText("35 Kg");
                                    textView_3.setText("3.5 Kg");
                                }
                                if(checkedId == btn_2.getId())
                                {
                                    textView_1.setText("5.5 Kg");
                                    textView_2.setText("20 Kg");
                                    textView_3.setText("3 Kg");
                                }
                                if(checkedId == btn_3.getId())
                                {
                                    textView_1.setText("8.8 Kg");
                                    textView_2.setText("2 Kg");
                                    textView_3.setText("4.5 Kg");
                                }
                            }
                        });
                        break;
                    case "15":
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if(checkedId == btn_1.getId())
                                {
                                    textView_1.setText("4.8 Kg");
                                    textView_2.setText("35 Kg");
                                    textView_3.setText("3.5 Kg");
                                }
                                if(checkedId == btn_2.getId())
                                {
                                    textView_1.setText("6.7 Kg");
                                    textView_2.setText("20 Kg");
                                    textView_3.setText("3 Kg");
                                }
                                if(checkedId == btn_3.getId())
                                {
                                    textView_1.setText("9.8 Kg");
                                    textView_2.setText("2 Kg");
                                    textView_3.setText("4.5 Kg");
                                }
                            }
                        });
                        break;
                    case "20":
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if(checkedId == btn_1.getId())
                                {
                                    textView_1.setText("7 Kg");
                                    textView_2.setText("35 Kg");
                                    textView_3.setText("3.5 Kg");
                                }
                                if(checkedId == btn_2.getId())
                                {
                                    textView_1.setText("9.3 Kg");
                                    textView_2.setText("20 Kg");
                                    textView_3.setText("3 Kg");
                                }
                                if(checkedId == btn_3.getId())
                                {
                                    textView_1.setText("12 Kg");
                                    textView_2.setText("2 Kg");
                                    textView_3.setText("5 Kg");
                                }
                            }
                        });
                        break;
                    default:
                        Toast.makeText(Cow.this, "Some Error Occured", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
