package com.example.NutriPhi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Feeding_Cattle extends AppCompatActivity {
    CardView cardView_1, cardView_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeding__cattle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        cardView_1 = findViewById(R.id.cow);
        cardView_2 = findViewById(R.id.buffalo);

        cardView_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Feeding_Cattle.this, Cow.class));
            }
        });

        cardView_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Feeding_Cattle.this, Buffalo.class));
            }
        });
    }
}
