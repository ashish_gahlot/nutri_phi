package com.example.NutriPhi;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.firebase.client.Firebase;

public class Feedback extends AppCompatActivity {
    EditText name, feedback_data;
    Button btn_submit;
    Firebase firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeback);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Firebase.setAndroidContext(this);

        String UniqueID =
                Settings.Secure.getString(getApplicationContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID);

        name = findViewById(R.id.editText3);
        feedback_data = findViewById(R.id.editText2);

        firebase = new Firebase("https://loginsystem-5edc8.firebaseio.com/Users" +UniqueID);

        btn_submit = findViewById(R.id.button2);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String na_me = name.getText().toString();
                final String data = feedback_data.getText().toString();

                Firebase child_name = firebase.child("Name");
                child_name.setValue(na_me);
                if(na_me.isEmpty())
                {
                    name.setError("This is an Required Field");
                    btn_submit.setEnabled(false);
                }
                else
                {
                    name.setError(null);
                    btn_submit.setEnabled(true);
                }

                Firebase child_feedback_data = firebase.child("Message");
                child_feedback_data.setValue(data);
                if(data.isEmpty())
                {
                    feedback_data.setError("This is an Required Field");
                    feedback_data.setEnabled(false);
                }
                else
                {
                    feedback_data.setError(null);
                    feedback_data.setEnabled(true);
                }
            }
        });

    }
}
